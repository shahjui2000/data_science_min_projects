## Minimal projects in Python(Jupyter Notebooks) 

#### What i did so far...

- data_processing/
	- keywords_extraction_nlp/
		- **keyword_extraction.ipynb**:
			- Natural processing language to process text-information inside of a dataset.
	
	- **kaggle_data_to_colab.ipynb**:
		- A simple, fast way to transfer Kaggle data into Google Colab content folder.
  
		- A kaggle accout token is needed to make the code work.


- deep_learning/
	- neural_networks/
		- **gradient_descent.ipynb**:
			- First implementation of a gradient descent using `udacity` support.
			
- machine_learning/ 
	- lin_regression/
		- **lin_regression_planes_crash.ipynb**:
			- A train and predict using linear regression of total planes crashes using categorized values a dataset.

		- **lin_regression_sweeden_population.ipynb**:
			- Simple aplication of linear model into a data of Sweeden population to predict the growht of it in the next years.
    

